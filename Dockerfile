FROM webdevops/php-nginx:8.3-alpine

# Install required dependencies for Laravel
RUN apk add --no-cache oniguruma-dev libxml2-dev \
    && docker-php-ext-install bcmath ctype fileinfo mbstring pdo_mysql xml

# Install Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# Install Node.js and npm
RUN apk add --no-cache nodejs npm

# Set environment variables
ENV WEB_DOCUMENT_ROOT /app/public
ENV APP_ENV production

# Set the working directory
WORKDIR /app

# Copy application files
COPY . .

# Copy .env.example to .env
RUN cp -n .env.example .env

# Install PHP dependencies and optimize the application
RUN composer install --no-interaction --optimize-autoloader --no-dev \
    && php artisan key:generate \
    && php artisan config:cache \
    && php artisan route:cache \
    && php artisan view:cache

# Install Node.js dependencies and build assets
RUN npm install \
    && npm run build

# Change ownership of the application files
RUN chown -R application:application .

# Expose the necessary port (Nginx default)
EXPOSE 80

# Command to run the application
CMD ["supervisord"]
