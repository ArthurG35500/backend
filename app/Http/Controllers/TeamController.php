<?php

namespace App\Http\Controllers;

use App\Models\Team;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/teams",
     *     @OA\Response(response="200", description="Liste des équipes")
     * )
     */
    public function index()
    {
        return Team::all();
    }

    /**
     * @OA\Post(
     *     path="/api/teams",
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/Team")
     *     ),
     *     @OA\Response(response="201", description="Équipe créée avec succès")
     * )
     */
    public function store(Request $request)
    {
        $team = Team::create($request->all());
        return response()->json($team, 201);
    }

    /**
     * @OA\Get(
     *     path="/api/teams/{id}",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(response="200", description="Détails de l'équipe")
     * )
     */
    public function show($id)
    {
        return Team::findOrFail($id);
    }

    /**
     * @OA\Put(
     *     path="/api/teams/{id}",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/Team")
     *     ),
     *     @OA\Response(response="200", description="Équipe mise à jour avec succès")
     * )
     */
    public function update(Request $request, $id)
    {
        $team = Team::findOrFail($id);
        $team->update($request->all());
        return response()->json($team, 200);
    }

    /**
     * @OA\Delete(
     *     path="/api/teams/{id}",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(response="204", description="Équipe supprimée avec succès")
     * )
     */
    public function destroy($id)
    {
        Team::findOrFail($id)->delete();
        return response()->json(null, 204);
    }
}
