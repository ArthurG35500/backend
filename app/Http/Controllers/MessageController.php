<?php

namespace App\Http\Controllers;

use App\Models\Message;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/messages",
     *     @OA\Response(response="200", description="Liste des messages")
     * )
     */
    public function index()
    {
        return Message::all();
    }

    /**
     * @OA\Post(
     *     path="/api/messages",
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/Message")
     *     ),
     *     @OA\Response(response="201", description="Message créé avec succès")
     * )
     */
    public function store(Request $request)
    {
        $message = Message::create($request->all());
        return response()->json($message, 201);
    }

    /**
     * @OA\Get(
     *     path="/api/messages/{id}",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(response="200", description="Détails du message")
     * )
     */
    public function show($id)
    {
        return Message::findOrFail($id);
    }

    /**
     * @OA\Put(
     *     path="/api/messages/{id}",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/Message")
     *     ),
     *     @OA\Response(response="200", description="Message mis à jour avec succès")
     * )
     */
    public function update(Request $request, $id)
    {
        $message = Message::findOrFail($id);
        $message->update($request->all());
        return response()->json($message, 200);
    }

    /**
     * @OA\Delete(
     *     path="/api/messages/{id}",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(response="204", description="Message supprimé avec succès")
     * )
     */
    public function destroy($id)
    {
        Message::findOrFail($id)->delete();
        return response()->json(null, 204);
    }
}
