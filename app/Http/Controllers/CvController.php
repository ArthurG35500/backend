<?php

namespace App\Http\Controllers;

use App\Models\Cv;
use Illuminate\Http\Request;

class CvController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/cvs",
     *     @OA\Response(response="200", description="Liste des CVs")
     * )
     */
    public function index()
    {
        return Cv::all();
    }

    /**
     * @OA\Post(
     *     path="/api/cvs",
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/Cv")
     *     ),
     *     @OA\Response(response="201", description="CV créé avec succès")
     * )
     */
    public function store(Request $request)
    {
        $cv = Cv::create($request->all());
        return response()->json($cv, 201);
    }

    /**
     * @OA\Get(
     *     path="/api/cvs/{id}",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Détails du CV",
     *         @OA\JsonContent(ref="#/components/schemas/Cv")
     *     )
     * )
     */
    public function show($id)
    {
        return Cv::findOrFail($id);
    }

    /**
     * @OA\Put(
     *     path="/api/cvs/{id}",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/Cv")
     *     ),
     *     @OA\Response(response="200", description="CV mis à jour avec succès")
     * )
     */
    public function update(Request $request, $id)
    {
        $cv = Cv::findOrFail($id);
        $cv->update($request->all());
        return response()->json($cv, 200);
    }

    /**
     * @OA\Delete(
     *     path="/api/cvs/{id}",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(response="204", description="CV supprimé avec succès")
     * )
     */
    public function destroy($id)
    {
        Cv::findOrFail($id)->delete();
        return response()->json(null, 204);
    }
}
