<?php

namespace App\OpenApi;

/**
 * @OA\OpenApi(
 *     @OA\Info(
 *         title="API Gestion Recrutement",
 *         version="1.0",
 *         description="API documentation for the Gestion Recrutement application"
 *     ),
 *     @OA\Server(
 *         url="http://localhost:8000/api",
 *         description="Local server"
 *     ),
 *     @OA\Components(
 *         @OA\Schema(
 *             schema="Cv",
 *             type="object",
 *             title="Cv",
 *             properties={
 *                 @OA\Property(property="first_name", type="string"),
 *                 @OA\Property(property="last_name", type="string"),
 *                 @OA\Property(property="phone", type="string"),
 *                 @OA\Property(property="email", type="string"),
 *                 @OA\Property(property="linkedin_profile", type="string"),
 *                 @OA\Property(property="file_path", type="string"),
 *                 @OA\Property(property="user_id", type="integer"),
 *             }
 *         ),
 *         @OA\Schema(
 *             schema="Message",
 *             type="object",
 *             title="Message",
 *             properties={
 *                 @OA\Property(property="content", type="string"),
 *                 @OA\Property(property="start_date", type="string", format="date"),
 *                 @OA\Property(property="end_date", type="string", format="date"),
 *             }
 *         ),
 *         @OA\Schema(
 *             schema="Team",
 *             type="object",
 *             title="Team",
 *             properties={
 *                 @OA\Property(property="name", type="string"),
 *             }
 *         ),
 *         @OA\Schema(
 *             schema="User",
 *             type="object",
 *             title="User",
 *             properties={
 *                 @OA\Property(property="name", type="string"),
 *                 @OA\Property(property="email", type="string"),
 *                 @OA\Property(property="password", type="string"),
 *             }
 *         )
 *     )
 * )
 */
class SwaggerConfig
{
    // Ce fichier est uniquement pour la configuration Swagger.
}
